import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class RMIReplicaManager {
	static Registry r=null;
	static PlacesListManagerInterface placeListHosts=null;
	public static void main(String[] args) {
		
		try{
			r = LocateRegistry.createRegistry(Integer.parseInt(args[0]));
		}catch(RemoteException a){
			a.printStackTrace();
		}
		
		try{
			placeListHosts = new ReplicaManager(Integer.parseInt(args[0]));	
			r.rebind("replicamanager", placeListHosts );

			System.out.println("ReplicaManager server ready");
		}catch(Exception e) {
			System.out.println("ReplicaManager server main " + e.getMessage());}
	}

}

