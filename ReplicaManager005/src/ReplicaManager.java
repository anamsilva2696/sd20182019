import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class ReplicaManager extends UnicastRemoteObject implements PlacesListManagerInterface, ReplicasManagementInterface{

	private static final long serialVersionUID = 1L;
	private final ReplicaManager r;
	private static PlacesListManagerInterface rep;
	private static ManagerInterface m = null;
	private ArrayList<String> placesList = new ArrayList<String>();
	private ArrayList<String> replicaList = new ArrayList<String>();

	public ReplicaManager(int port) throws RemoteException, InterruptedException {
		
		//liga te ao manger e diz que existe parecido com o place manager 
		
		
		try {
			m = (ManagerInterface) Naming.lookup("rmi://localhost:9999/manager");
		
			String address;
			System.out.println("tenta iniciar o replica na porta " + port);
			address = m.addToManager("rmi://localhost:" + port + "/replicamanager");//1� ponto //adiciona-se ao manager e pede lhe um adress de outro replica manager que j� existe
			System.out.println(address);
			ArrayList<String> temp= new ArrayList<String>(); //3� ponto
			ArrayList<String> lista= new ArrayList<String>();
			if ( address != null) { //se houver um outro replica manager
				rep = (PlacesListManagerInterface) Naming.lookup(address); //liga se a esse replica manager
				
				lista = m.ShowList();
				temp = rep.getReplicaPlacesList(); //pede lhe a lista de servidores que ele tenha ligados a ele
				
				for(String s: temp) {
					placesList.add(s); //copia paraa si proprio. adicionar � sua propria lista todos os places que l� est�o no outro replica manager
				}
				
					System.out.println("Tem estas replicas");
					System.out.println(lista);
				
			}		
		} 
		catch (Exception e) {
			e.printStackTrace();
		} //liga se ao manager
		
		
		
		
		
		
		
		r = this;
		(new Thread() {
			public void run() {
				while(true) {
					// vamos utilizar o lock do ReplicaManager
					//se utilizarmos o "this" estamos a recorrer ao lock do object Thread
					synchronized(r) {  
						for(String s: placesList) {
							MonitoringInterface m = null;
							try {
								m = (MonitoringInterface) Naming.lookup(s);
								m.ping();
								System.out.println("ReplicaManager " + port + " -->PING "+ s);
							}catch(Exception e) {
								placesList.remove(s);
								(new Thread() {
									public void run() {
										RMIServer.main(new String[]{s.split(":")[2].substring(0, 4)});
									}
								}).start();
								System.out.println("ReplicaManager-->RECUPEREI");
								break; //a lista foi modificada e por isso terminamos o FOR
							}
						}
					}
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}}}).start();
		
		
		

	}

	public synchronized void addPlace(Place p) throws RemoteException { 
		PlacesListInterface l;
		ObjectRegistryInterface r;
		try {
			for(String s:placesList) {
				l = (PlacesListInterface) Naming.lookup(s);
				l.addPlace(p);
			}
			r = (ObjectRegistryInterface) Naming.lookup("rmi://localhost:2023/registry");
			r.addRManager(p.getPostalCode(), "rmi://localhost:2024/replicamanager");
		} catch (MalformedURLException | NotBoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	public synchronized String getPlaceListAddress(String id) throws RemoteException {
		if(placesList.isEmpty()) return null;
		return placesList.get(ThreadLocalRandom.current().nextInt(0, placesList.size()));
	}

	@Override
	public synchronized String addReplica(String replicaAddress) throws RemoteException {
		System.out.println("ReplicaManager-->Adicionei uma Réplica servidor");
		String add = getPlaceListAddress(null);
		this.placesList.add(replicaAddress);
		return add;
	}
	@Override
	public synchronized String getReplicaListAddress(String id) throws RemoteException {
		if(replicaList.isEmpty()) return null;
		return replicaList.get(ThreadLocalRandom.current().nextInt(0, replicaList.size()));
	}
	
	
	@Override
	public synchronized String addReplicaReplica(String replicaAddress) throws RemoteException {
		System.out.println("ReplicaManager-->Adicionei uma Réplica da replica");
		String add = getPlaceListAddress(null);
		this.replicaList.add(replicaAddress);
		return add;
	}

	@Override
	public synchronized void removeReplica(String replicaAddress) throws RemoteException {
		this.placesList.remove(replicaAddress);
	}
	
	@Override
	public ArrayList<String> getReplicaPlacesList(){
		return placesList;
	}
}
