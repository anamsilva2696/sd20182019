import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ObjectRegistryInterface extends Remote{
	
	public void addRManager(String objectID,String serverAddress) throws RemoteException;	
	public String resolve(String objectID) throws RemoteException;
}
