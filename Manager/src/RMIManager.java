import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class RMIManager {
	static Registry r=null;
	static ManagerInterface manager=null;
	public static void main(String[] args) {
		
		try{
			r = LocateRegistry.createRegistry(9999);
		}catch(RemoteException a){
			a.printStackTrace();
		}
		
		try{
			manager = new Manager();	
			r.rebind("manager", manager );

			System.out.println("Manager server ready");
		}catch(Exception e) {
			System.out.println("Manager server main " + e.getMessage());}
	}

}

