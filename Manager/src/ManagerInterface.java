import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface ManagerInterface extends Remote {

	public String addToManager(String caminho) throws RemoteException;
	public ArrayList<String> ShowList() throws RemoteException;

}
